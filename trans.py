import cv2
import numpy as np
import sys

from poi import get_landmarks

im = cv2.imread(sys.argv[1])
cap = cv2.imread(sys.argv[2])
rows, cols = im.shape[:2]

im_pts = np.array(get_landmarks(im), dtype=np.float32)
im_pts = np.array([ im_pts[36], im_pts[45], im_pts[30] ])
print im_pts

# cap_pts = np.array([(80, 217.), (221., 222.), (146., 259.)], dtype=np.float32)
cap_pts = np.array([(121., 226.), (189., 225.), (155., 257.)], dtype=np.float32)

M = cv2.getAffineTransform(cap_pts, im_pts)

dst = cv2.warpAffine(cap, M, (cols, rows),
        borderMode=cv2.BORDER_CONSTANT, borderValue=(255,255,255,0.3))
# cv2.imshow('inter', dst)

roi = im[0:rows, 0:cols]
img2gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
# _, mask2 = cv2.threshold(img2gray, 0, 255, cv2.THRESH_BINARY_INV)
# mask = cv2.bitwise_or(mask, mask2)
mask_inv = cv2.bitwise_not(mask)

im_bg = cv2.bitwise_and(roi, roi, mask=mask)
cap_fg = cv2.bitwise_and(dst, dst, mask=mask_inv)

res = cv2.add(im_bg, cap_fg)

cv2.imshow('dst', res)
cv2.waitKey(0)
cv2.destroyAllWindows()
