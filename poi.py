import cv2
import dlib
import numpy as np

import sys

PREDICTOR_PATH = "/home/chengcai/Image/shape_predictor_68_face_landmarks.dat"

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(PREDICTOR_PATH)

class TooManyFaces(Exception):
    pass

class NoFaces(Exception):
    pass

def get_landmarks(im):
    rects = detector(im, 1)

    if len(rects) > 1:
        raise TooManyFaces
    if len(rects) == 0:
        raise NoFaces

    return np.matrix([[p.x, p.y] for p in predictor(im, rects[0]).parts()])

# im = cv2.imread(sys.argv[1])
# mat = get_landmarks(im)
# print mat[0], mat[16], mat[30]

